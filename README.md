Brookwood Apartments in Covina, CA nestled at the foot of the San Gabriel Mountains, about 20 miles east of Los Angeles, in the heart of the San Gabriel Valley. You’ll love our proximity to freeways, the Metrolink station, Citrus College, and APU.

Address: 18537 Arrow Highway, Covina, CA 91722, USA

Phone: 626-376-9842